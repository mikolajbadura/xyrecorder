﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.Description" Type="Str">Supports models 3631 through 3634.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*"!!!*Q(C=\&gt;3^&lt;2N"%-8R*]/"5X:A8/JQ!D@!&amp;ND#*#S!K2)$&lt;'&amp;;)!-F&gt;M17HENA#X1*^0_7)U'1!$+2!"P1H:9@&lt;X&gt;H@^Q\H43/,^*HH7]&gt;[[M(`&gt;*[0:&gt;&lt;PWK=D`XTW0%SUM@XO6`^]@8]?3$^FV)PZLW=@W8^'XZ&gt;04\[0`L`\`Y&lt;"W0O&lt;QZ[ZU/['V+;;56,7IR@&gt;T;ZS5VO=J/&lt;P-C,P-C,P-C,0-G40-G40-G40-C$0-C$0-C$@*TE)B?ZS.E6M\B:S"1V"=RA+$)PRG-]RG-]@*8R')`R')`R-%4'9TT'9TT'QT1:D`%9D`%9$[7'R/-ERW-]F&amp;@B+4S&amp;J`!5(J:5Y3E!R7*&amp;Y;))$"7&gt;R9@#5XA+$R^6?!J0Y3E]B9&gt;O&amp;:\#5XA+4_&amp;BSNC6'JLZ*-&gt;$'37?R*.Y%E`CI&lt;134_**0)EH]&lt;#=%E`C32$*AEFR#%IG*1/3,YEH]@#GR*.Y%E`C34RUD3O59W&gt;GT8S3YQE]A3@Q"*\!1QE&amp;HM!4?!*0Y+'M!E`A#4S"*`#QF!*0Y!E]!329F/56&amp;!MG"I/#)0$Q._[7'&amp;?JBC4'W@_;FRN6@1/K&lt;STV$;/_%&gt;187(XBV"&gt;%P&gt;(K$62PD0I(KX_)'KB?7&amp;V106!H8I_U!WV(W^)WN"6N3:NICXHK'Q]]H5Y[(I][(!\;\8&lt;;&lt;L@;&lt;$:;L6:;,J?;JEG,R?,J;@76]X,=08]O?:K_`@\TM0]V4&gt;^`0/TX0`E_Z`?-?[^W?3\^#]^'@&gt;,ZW7/?0@I,8,A\;!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.1.1.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
</Library>
