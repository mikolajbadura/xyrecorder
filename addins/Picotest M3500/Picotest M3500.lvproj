<?xml version='1.0'?>
<Project Name="Template - DMM.lvproj" Type="Project" LVVersion="8208000" URL="/&lt;instrlib&gt;/_Template - DMM/Template - DMM.lvproj">
   <Property Name="Instrument Driver" Type="Str">True</Property>
   <Property Name="NI.Project.Description" Type="Str">This project is used by developers to edit API and example files for LabVIEW Plug and Play instrument drivers.</Property>
   <Item Name="My Computer" Type="My Computer">
      <Property Name="CCSymbols" Type="Str">OS,Win;CPU,x86;</Property>
      <Property Name="specify.custom.address" Type="Bool">false</Property>
      <Item Name="Examples" Type="Folder">
         <Item Name="Picotest M3500.bin3" Type="Document" URL="/&lt;instrlib&gt;/Picotest M3500/Examples/Picotest M3500.bin3"/>
         <Item Name="Picotest M3500 Configure Math Limits.vi" Type="VI" URL="/&lt;instrlib&gt;/Picotest m3500/Picotest M3500/Examples/Picotest M3500 Configure Math Limits.vi"/>
         <Item Name="Picotest M3500 Read Scanner Card(10 Channels).vi" Type="VI" URL="/&lt;instrlib&gt;/Picotest m3500/Picotest M3500/Examples/Picotest M3500 Read Scanner Card(10 Channels).vi"/>
         <Item Name="Picotest M3500 Read Scanner Card(20 Channels).vi" Type="VI" URL="/&lt;instrlib&gt;/Picotest m3500/Picotest M3500/Examples/Picotest M3500 Read Scanner Card(20 Channels).vi"/>
         <Item Name="Picotest M3500 Read Multiple.vi" Type="VI" URL="/&lt;instrlib&gt;/Picotest m3500/Picotest M3500/Examples/Picotest M3500 Read Multiple.vi"/>
         <Item Name="Picotest M3500 Read Single.vi" Type="VI" URL="/&lt;instrlib&gt;/Picotest m3500/Picotest M3500/Examples/Picotest M3500 Read Single.vi"/>
         <Item Name="Picotest M3500 Read SPRTD Temperature.vi" Type="VI" URL="/&lt;instrlib&gt;/Picotest m3500/Picotest M3500/Examples/Picotest M3500 Read SPRTD Temperature.vi"/>
         <Item Name="Picotest M3500 Read Temperature.vi" Type="VI" URL="/&lt;instrlib&gt;/Picotest m3500/Picotest M3500/Examples/Picotest M3500 Read Temperature.vi"/>
         <Item Name="Picotest M3500 Read Triggered Multiple.vi" Type="VI" URL="/&lt;instrlib&gt;/Picotest m3500/Picotest M3500/Examples/Picotest M3500 Read Triggered Multiple.vi"/>
         <Item Name="Picotest M3500 Read User Defined Temperature.vi" Type="VI" URL="/&lt;instrlib&gt;/Picotest m3500/Picotest M3500/Examples/Picotest M3500 Read User Defined Temperature.vi"/>
      </Item>
      <Item Name="Picotest M3500.lvlib" Type="Library" URL="Picotest M3500.lvlib">
         <Item Name="Public" Type="Folder">
            <Item Name="Configure" Type="Folder">
               <Item Name="Low Level" Type="Folder">
                  <Item Name="Configure_Low Level.mnu" Type="Document" URL="/&lt;instrlib&gt;/Picotest M3500/Public/Configure/Low Level/Configure_Low Level.mnu"/>
                  <Item Name="Configure Multipoint.vi" Type="VI" URL="Public/Configure/Low Level/Configure Multipoint.vi"/>
                  <Item Name="Configure Trigger.vi" Type="VI" URL="Public/Configure/Low Level/Configure Trigger.vi"/>
               </Item>
               <Item Name="Configure.mnu" Type="Document" URL="/&lt;instrlib&gt;/Picotest M3500/Public/Configure/Configure.mnu"/>
               <Item Name="Configure AC BandWidth.vi" Type="VI" URL="Public/Configure/Configure AC BandWidth.vi"/>
               <Item Name="Configure Aperture.vi" Type="VI" URL="Public/Configure/Configure Aperture.vi"/>
               <Item Name="Configure Autozero.vi" Type="VI" URL="Public/Configure/Configure Autozero.vi"/>
               <Item Name="Configure Average.vi" Type="VI" URL="Public/Configure/Configure Average.vi"/>
               <Item Name="Configure DC Input Resistance.vi" Type="VI" URL="Public/Configure/Configure DC Input Resistance.vi"/>
               <Item Name="Configure Digital Filter.vi" Type="VI" URL="Public/Configure/Configure Digital Filter.vi"/>
               <Item Name="Configure Math.vi" Type="VI" URL="Public/Configure/Configure Math.vi"/>
               <Item Name="Configure Measurement.vi" Type="VI" URL="Public/Configure/Configure Measurement.vi"/>
               <Item Name="Configure Scanner Card(10 Channels).vi" Type="VI" URL="Public/Configure/Configure Scanner Card(10 Channels).vi"/>
               <Item Name="Configure Scanner Card(20 Channels).vi" Type="VI" URL="Public/Configure/Configure Scanner Card(20 Channels).vi"/>
               <Item Name="Configure Scanner Card.vi" Type="VI" URL="Public/Configure/Configure Scanner Card.vi"/>
               <Item Name="Configure Temperature.vi" Type="VI" URL="Public/Configure/Configure Temperature.vi"/>
               <Item Name="Configure Thermocouple.vi" Type="VI" URL="Public/Configure/Configure Thermocouple.vi"/>
            </Item>
            <Item Name="Data" Type="Folder">
               <Item Name="Low Level" Type="Folder">
                  <Item Name="Data_Low Level.mnu" Type="Document" URL="/&lt;instrlib&gt;/Picotest M3500/Public/Data/Low Level/Data_Low Level.mnu"/>
                  <Item Name="Initiate.vi" Type="VI" URL="Public/Data/Low Level/Initiate.vi"/>
               </Item>
               <Item Name="Data.mnu" Type="Document" URL="/&lt;instrlib&gt;/Picotest M3500/Public/Data/Data.mnu"/>
               <Item Name="Read (Multiple Points).vi" Type="VI" URL="Public/Data/Read (Multiple Points).vi"/>
               <Item Name="Read (Single Point).vi" Type="VI" URL="Public/Data/Read (Single Point).vi"/>
               <Item Name="Fetch Multiple Points.vi" Type="VI" URL="Public/Data/Low Level/Fetch Multiple Points.vi"/>
               <Item Name="Read.vi" Type="VI" URL="Public/Data/Read.vi"/>
            </Item>
            <Item Name="Utility" Type="Folder">
               <Item Name="Utility.mnu" Type="Document" URL="/&lt;instrlib&gt;/Picotest M3500/Public/Utility/Utility.mnu"/>
               <Item Name="Error Query.vi" Type="VI" URL="Public/Utility/Error Query.vi"/>
               <Item Name="Reset.vi" Type="VI" URL="Public/Utility/Reset.vi"/>
               <Item Name="Revision Query.vi" Type="VI" URL="/&lt;instrlib&gt;/Picotest M3500/Public/Utility/Revision Query.vi"/>
            </Item>
            <Item Name="dir.mnu" Type="Document" URL="/&lt;instrlib&gt;/Picotest M3500/Public/dir.mnu"/>
            <Item Name="VI Tree.vi" Type="VI" URL="/&lt;instrlib&gt;/Picotest M3500/Public/VI Tree.vi"/>
            <Item Name="Close.vi" Type="VI" URL="Public/Close.vi"/>
            <Item Name="Initialize.vi" Type="VI" URL="Public/Initialize.vi"/>
         </Item>
         <Item Name="Private" Type="Folder">
            <Item Name="Default Instrument Setup.vi" Type="VI" URL="Private/Default Instrument Setup.vi"/>
            <Item Name="OVLD check.vi" Type="VI" URL="Private/OVLD check.vi"/>
         </Item>
         <Item Name="Picotest M3500 Readme.html" Type="Document" URL="/&lt;instrlib&gt;/Picotest M3500/Picotest M3500 Readme.html"/>
      </Item>
      <Item Name="Dependencies" Type="Dependencies"/>
      <Item Name="Build Specifications" Type="Build"/>
   </Item>
</Project>
