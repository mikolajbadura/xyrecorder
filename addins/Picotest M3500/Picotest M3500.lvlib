﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.Description" Type="Str">LabVIEW Plug and Play instrument driver for Picotest Corp., M3500A, Digital MultiMeter.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*(!!!*Q(C=\&gt;1^&lt;NN!%)&lt;BTU'+F&amp;(DWN!6ZAK[AKYQ6V$LTLK"-3=)I!OE5/&gt;[+P&lt;K5K6A&amp;;C)#`HF=*)Y,K15-:!!8HJJ]&gt;O`2UO+5J50UHO&gt;,J8DW4+V$]?K!_8F;7[@,KLX5(W0&gt;4W(]`DBR@DO/@QW`Y`TL^($H[V`XK_TZ;X^L@X`&lt;L^1[(.\M&gt;-L&amp;_GKJ.3E"N7J6N`OF/2*HO2*HO2*(O2"(O2"(O2"\O2/\O2/\O2/&lt;O2'&lt;O2'&lt;O2'8A?ZS%5O=H9F74R:+*EUG3$J$%8*+@%EHM34?,B5YEE]C3@R*"[[+0%EHM34?")0QZ2Y%E`C34S*B[F+EH71YUE]4+`!%XA#4_!*0#SJQ"-!AM7#C9.*9#BI$$Y%HM!4?0CIQ".Y!E`A#4QU+`!%HM!4?!)01WJ8ID4419[(;?2Y()`D=4S/B[HF?"S0YX%]DI@FZ(A=DY.Q&amp;H1GBS"HE.0"O8!]DI&gt;`=DS/R`%Y(M&gt;$5^UBLZW:..."DM@Q'"\$9XA-$V0)]"A?QW.Y$!`4SP!9(M.D?!Q03]HQ'"\$9U#-26F?RG4'1+/4%2A?`OJJM&lt;J,52+LIX_;]Y/K@A$6$Z&lt;[A6%`#/I&lt;L,ZR[BOCXGDV"KIX2PW&amp;V6^%$61PL*Z1X6%DZQ.V4^V2N^1.&gt;5V&gt;5:@5R44U,X==RV'(QU(\`6[\X5\&lt;\6;&lt;T5&lt;L^6KLV5L,Z6+,R?,HW_K'9SZ8T^^,Y]0(OS`@PXX[_H$^?8Q=\K@L+&lt;_FXWP6_&lt;XU,\Q&lt;^5[H:[^Z^OA*G2$*0A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.5.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
</Library>
