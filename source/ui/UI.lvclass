﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"&amp;L5F.31QU+!!.-6E.$4%*76Q!!$6Q!!!10!!!!)!!!$4Q!!!!0!!!!!1J633ZM&gt;G.M98.T!!!!!+!8!)!!!$!!!#A!!!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!4G&amp;=FSYXNESS'7&lt;JJ/A'JQ!!!!Q!!!!1!!!!!&amp;W8VPYUQ#6#BWP__D.1@?X5(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!":"%H:RZ;E3*ZG8_`2T'D2!1!!!0`````5(9T:DQ#S"/G!#:DM_%*_!!!!%/&gt;D#.#,@KEB8.&lt;;+N&amp;E8'E!!!!%!!!!!!!!!#=!!5R71U-!!!!"!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!-!!!!!!A!"!!!!!!!C!!!!((C=9W"D9'JAO-!!R)R!T.4!^!0)`A$C-Q!!;!%).A!!!!!!21!!!2BYH'.AQ!4`A1")-4)Q-*U!UCRIYG!;RK9GQ'5O,LOAYMR1.\*#2"G"9ER\A!QGE"SK8J"`Q(Z#!7R9T!9!&gt;P5I&amp;1!!!!!!!!Q!!6:*2&amp;-!!!!!!!-!!!&amp;E!!!#T(C=3W"E9-AUND$&lt;!+3:A6C=I9%B/4]FF9M"S'?!A#V-$"3$!+BZ7GDCBA=/JQ'"(L^]#ZD@`);HWU6&amp;I,F'29+J6+4&lt;2U7EUU?&amp;J:.&amp;Z=7@````.R`B/&gt;TNE80=U1;ENJM$+(\=295$R!(3,#$[@W!'3"8-P!#A;2Q.&amp;=I-*3S'"[)/(W]Q9929$$-S#N8_(3$JQ]DOY1![1O$A1Z&lt;O2AUAPX=CC!1+]83'=%A=&gt;_(1%10S'5^U!KXPZ)'ZEA.O@RD)A")6A5Y4E%N:1+;$V83T(8@1!,P&lt;112#:5#I#AB6!(9-W!6(//)/Q].L\?N\OU$BS)95BAZ!X!$%I$B%RHI-D!QA#ZG!:#V5L1W1T116A]5&amp;C(U"SN:!UP-&amp;S8S1(J$-'KA9C,U*SG[!OA=E^B&gt;)4Y#S1&lt;Z.A,+ZA?Q&amp;5,91E#U!:5M#W1_A&lt;$EI?Q-UCH$2TPYOLED?B[&gt;0!$5&gt;=I!!!!!-&amp;Q#!"1!!"$%X,D!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Q8!)!&amp;!!!%-4=O-!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$"=!A!5!!!1R.SYQ!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9"A!!'"G!!"BA9!!:A"A!'A!%!"M!$!!;Q$1!'D$M!"I06!!;!KQ!'A.5!"I#L!!;!V1!'A+M!"I$6!!:ALA!''.A!"A&lt;A!!9"A!!(`````!!!%!0```````````````````````````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!$U^!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!$WSB\+S01!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!$WSBW*C9G+SMDU!!!!!!!!!!!!!!!!!!!!!``]!!$WSBW*C9G*C9G*CML)^!!!!!!!!!!!!!!!!!!$``Q#SBW*C9G*C9G*C9G*C9L+S!!!!!!!!!!!!!!!!!0``!)?(9G*C9G*C9G*C9G*C`L)!!!!!!!!!!!!!!!!!``]!B\+SBW*C9G*C9G*C`P\_BQ!!!!!!!!!!!!!!!!$``Q#(ML+SMI&gt;C9G*C`P\_`P[(!!!!!!!!!!!!!!!!!0``!)?SML+SML+(MP\_`P\_`I=!!!!!!!!!!!!!!!!!``]!B\+SML+SML,_`P\_`P\_BQ!!!!!!!!!!!!!!!!$``Q#(ML+SML+SMP\_`P\_`P[(!!!!!!!!!!!!!!!!!0``!)?SML+SML+S`P\_`P\_`I=!!!!!!!!!!!!!!!!!``]!B\+SML+SML,_`P\_`P\_BQ!!!!!!!!!!!!!!!!$``Q#(ML+SML+SMP\_`P\_`P[(!!!!!!!!!!!!!!!!!0``!,+SML+SML+S`P\_`P\_ML)!!!!!!!!!!!!!!!!!``]!!)?(ML+SML,_`P\_ML+(!!!!!!!!!!!!!!!!!!$``Q!!!!#(ML+SMP\_ML+(!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!B\+SML*C!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!)&gt;C!!!!!!!!!!!!!!!!!!!!!!!!!!$```````````````````````````````````````````]!!!!-!!&amp;'5%B1!!!!!!!$!!!#;1!!"/*YH+W5T7M4524&amp;\QODP)17X[3J&gt;K!BM&lt;T')MH'TRK-FP:V53F&amp;C95O#D9Y]1.;IUV38&lt;7&lt;1=CC5!BE)84B*FM87&lt;C8Y'97OH*BQ;(Z#^Q5"4O*&gt;S;:G343&gt;'-#DU&gt;S@`@=O?=Q!.)0.O:LQL9*B"XB:&gt;'%A+I4A(K#1O=4?QNMG@Q"%F+)#4.UG2XYGG4=B#&amp;6D^&amp;LIA1`/^5E2&gt;[T1SQ^SR2M&amp;D$BH+J@E/^RA`%0Y\RUROEK1ZDNEK:PB5&gt;_U6UNBY+A46GHH#".)/+C*'E4^T-PMBKX@P5HK'+X^*P!B$[]R9V,W"'F0^ENS2[*IX3H*7$,+;D6;BYEN['90=9U-C2OD\MXA!EC]ZI&lt;=:M*W!TKJ"Q&gt;%7G7L&gt;ENK"]&gt;Y8K?'YC^=494&gt;[2%R*SRM5;DA2C?(?S:#3&amp;OT&amp;+&amp;(KL@S@!$\3-1)06(LA`SEG7#82N%(S;4B+8RHD&lt;BMK&lt;\VJDE&lt;,=):&gt;M&amp;S8(BNO8#L/W#8Z19?$;54\7B0B'?7S`G#^H.;/Z*^0&amp;[*J_0PNR]PJ5J:+.KJJ$ZV[#5U*7LVN.&lt;9OVMD))%LW#N?^EZK&amp;;LO!!]0@1/IC0==$H:@BL68&lt;DK,&gt;R3^4:X&amp;T=H@D^];GVP-OFT)AN?7+^A7/61`U!^9&lt;X_`].[1_DB\&lt;[QQCJ_.Q9%\W9&lt;]M++^1!&lt;M$K!G5:GJTOM&lt;26(:V"9&lt;QG&gt;^;&lt;69PP37KF5?DD=?&gt;*.KU2)/[X;V^:R[\A`M_"@[JK=(HQOOP`4."=NKJR=Q/:2&lt;-?%54%8`-)7M.M18;$T&lt;"^&gt;R&lt;@2?&lt;J0D_L@X0,[IHPF\S*U\#^FW&amp;;/!!!!!!!!"!!!!#!!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!!KA!!!!)!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!!.B=!A!!!!!!"!!A!-0````]!!1!!!!!!'A!!!!%!%E"1!!!+65EO&lt;(:D&lt;'&amp;T=Q!!!1!!!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=A!!!#58!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!!!!!!!!!!!!!!%!!)!#!!!!!1!!!"!!!!!+!!!!!)!!!1!!!!!!1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$6!!!"28C=D:$.;A*"%)3`W5H]770]/QNTS-'4F\T!EI#1GYD?8&gt;T&gt;%"B9=5@RG*&gt;-HC.ZAZ3LQ5-OI;#JLJ[OGBFAR"0@H]P*"R$&amp;KZ?J0WR]7F5.U5XQ4*)ZD*`^PALZTJ7&amp;K[&gt;OOXM\J#&amp;X72J3&lt;A6C-D"@'$8WUP)IWE[;W-+`UEL?&lt;&lt;E):S%P#`LSNDL?I'HX75&amp;]SL,BG'F\4E3,.A``SI_I&lt;61-7FML_I;:4"RX&gt;#7?=+_Q8ZA,`CL83;`'_3''PBT2B@2.9D%&gt;"AR6/T],I#\&lt;!!!!!!!!:1!"!!)!!Q!%!!!!3!!0!!!!!!!0!.A!V1!!!&amp;%!$Q!!!!!!$Q$9!.5!!!";!!]!!!!!!!]!W!$6!!!!9Y!!A!#!!!!0!.A!V1B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!!!!5F.31QU+!!.-6E.$4%*76Q!!$6Q!!!10!!!!)!!!$4Q!!!!!!!!!!!!!!#!!!!!U!!!%#!!!!"N-35*/!!!!!!!!!62-6F.3!!!!!!!!!7B36&amp;.(!!!!!!!!!8R$1V.5!!!!!!!!!:"-38:J!!!!!!!!!;2$4UZ1!!!!!!!!!&lt;B544AQ!!!!!!!!!=R%2E24!!!!!!!!!?"-372T!!!!!!!!!@2735.%!!!!!!!!!ABW:8*T!!!!"!!!!BR41V.3!!!!!!!!!I"(1V"3!!!!!!!!!J2*1U^/!!!!!!!!!KBJ9WQY!!!!!!!!!LR-37:Q!!!!!!!!!N"'5%BC!!!!!!!!!O2'5&amp;.&amp;!!!!!!!!!PB75%21!!!!!!!!!QR-37*E!!!!!!!!!S"#2%BC!!!!!!!!!T2#2&amp;.&amp;!!!!!!!!!UB73624!!!!!!!!!VR%6%B1!!!!!!!!!X".65F%!!!!!!!!!Y2)36.5!!!!!!!!!ZB71V21!!!!!!!!![R'6%&amp;#!!!!!!!!!]!!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"1!!!!!!!!!!0````]!!!!!!!!!O!!!!!!!!!!!`````Q!!!!!!!!$-!!!!!!!!!!$`````!!!!!!!!!.1!!!!!!!!!!0````]!!!!!!!!"!!!!!!!!!!!!`````Q!!!!!!!!%)!!!!!!!!!!$`````!!!!!!!!!4!!!!!!!!!!!0````]!!!!!!!!"@!!!!!!!!!!!`````Q!!!!!!!!'-!!!!!!!!!!4`````!!!!!!!!!P1!!!!!!!!!"`````]!!!!!!!!$"!!!!!!!!!!)`````Q!!!!!!!!-5!!!!!!!!!!H`````!!!!!!!!!S1!!!!!!!!!#P````]!!!!!!!!$.!!!!!!!!!!!`````Q!!!!!!!!.%!!!!!!!!!!$`````!!!!!!!!!VQ!!!!!!!!!!0````]!!!!!!!!$=!!!!!!!!!!!`````Q!!!!!!!!0U!!!!!!!!!!$`````!!!!!!!!"`A!!!!!!!!!!0````]!!!!!!!!)#!!!!!!!!!!!`````Q!!!!!!!!JY!!!!!!!!!!$`````!!!!!!!!#I!!!!!!!!!!!0````]!!!!!!!!+C!!!!!!!!!!!`````Q!!!!!!!!K9!!!!!!!!!!$`````!!!!!!!!#Q!!!!!!!!!!!0````]!!!!!!!!,#!!!!!!!!!!!`````Q!!!!!!!!OY!!!!!!!!!!$`````!!!!!!!!#]!!!!!!!!!!!0````]!!!!!!!!,S!!!!!!!!!!!`````Q!!!!!!!!PU!!!!!!!!!)$`````!!!!!!!!$.!!!!!!"F6*,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="UI.ctl" Type="Class Private Data" URL="UI.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="API" Type="Folder">
		<Item Name="test.vi" Type="VI" URL="../API/test.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#^!!!!"Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!6!$Q!!Q!!Q!%!!1!"!!%!!1!"!!%!!5!"!!%!!1#!!"Y!!!.#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!!!1!'!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574592</Property>
		</Item>
		<Item Name="Update Listbox.vi" Type="VI" URL="../API/Update Listbox.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#^!!!!"Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!6!$Q!!Q!!Q!%!!1!"!!%!!1!"!!%!!5!"!!%!!1$!!"Y!!!.#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!!!1!'!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
	</Item>
	<Item Name="subpanels" Type="Folder">
		<Item Name="analyze.vi" Type="VI" URL="../subpanels/analyze.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710290</Property>
		</Item>
		<Item Name="chart.vi" Type="VI" URL="../subpanels/chart.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="devices.vi" Type="VI" URL="../subpanels/devices.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="export.vi" Type="VI" URL="../subpanels/export.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
		</Item>
		<Item Name="listbox.vi" Type="VI" URL="../subpanels/listbox.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
	</Item>
	<Item Name="SubVIs" Type="Folder">
		<Item Name="Com References.vi" Type="VI" URL="../SubVIs/Com References.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;6!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````](476T=W&amp;H:1!+1&amp;-%2'&amp;U91!!#A"1!!)!"1!'!"B!=!!:!!%!"QR633"F&gt;G6O&gt;#"P&gt;81!!"R!=!!3!!%!"R&amp;.98.U:8)A=86F&gt;75A)'^V&gt;!!51#%/5G6T:81`)#B'97RT:3E!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;%"Q!"E!!1!(#&amp;6*)'6W:7ZU!!!91(!!%A!"!!=.47&amp;T&gt;'6S)(&amp;V:86F)!"5!0!!$!!$!!1!#!!*!!1!"!!%!!I!#Q!%!!Q!$1)!!(A!!!U)!!!!!!!!#1!!!!E!!!!!!!!!!!!!!!!!!!!)!!!!#!!!!!!!!!!)!!!!#!!!!!!"!!Y!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="Get Event Ref.vi" Type="VI" URL="../SubVIs/Get Event Ref.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$T!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````](1W^N&lt;7&amp;O:!!+1&amp;-%2'&amp;U91!!%E"1!!)!"1!'"UVF=X.B:W5!&amp;%"Q!"E!!1!(#&amp;6*)%6W:7ZU!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"J&lt;C!!6!$Q!!Q!!Q!%!!1!#!!%!!1!"!!%!!E!"!!%!!1#!!"Y!!!.#!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="Get Subpanels Paths.vi" Type="VI" URL="../SubVIs/Get Subpanels Paths.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%6!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!A!-0````]!)%"!!!(`````!!545X6C='&amp;O:7QA5G6G=S"/97VF=Q!71$,`````$7&amp;Q='6O:'6E)("B&gt;'A!'E"!!!(`````!!=.5X6C='&amp;O:7QA5G6G=Q!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;1!]!!-!!-!"!!'!!A!"!!%!!1!"!!*!!1!"!!%!Q!!?!!!$1A!!!!!!!!*!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821074</Property>
		</Item>
		<Item Name="Subpanels Refs Holder.vi" Type="VI" URL="../SubVIs/Subpanels Refs Holder.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Z!!!!%!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!'E"Q!!A!!!!#!!!-&gt;GEA=G6G:8*F&lt;G.F!!!;1%!!!@````]!"!V3:7:T)%RJ=X1A&lt;X6U!!A!-0````]!(%"!!!(`````!!9/4G&amp;N:8-A4'FT&gt;#"P&gt;81!!"2!=!!)!!!!!A!!"F:*)'^V&gt;!!!"!!!!"2!)1Z3:8.F&gt;$]A+%:B&lt;(.F+1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71%!!!@````]!"!F3:7:T)%RJ=X1!'%"!!!(`````!!9+4G&amp;N:8-A4'FT&gt;!!!$E!Q`````Q2/97VF!!"5!0!!$!!$!!5!"Q!)!!E!#1!*!!I!#Q!-!!U!$A-!!(A!!!U)!!!*!!!!#1!!!!E!!!!!!!!!!!!!!!!!!!!)!!!!#A!!!AA!!!))!!!##!!!!!!"!!]!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
	</Item>
	<Item Name="Lunch UI.vi" Type="VI" URL="../Lunch UI.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#^!!!!"Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!6!$Q!!Q!!Q!%!!1!"!!%!!1!"!!%!!5!"!!%!!1$!!"Y!!!.#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!!!1!'!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Main Window.vi" Type="VI" URL="../Main Window.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#H!!!!"A!%!!!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!!1!#!!-4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"5!0!!$!!!!!!!!!!!!!!!!!!!!!!!"!!!!!!!!!-!!(A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!!!!!!!!"!!5!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073775872</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
</LVClass>
